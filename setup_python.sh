#!/usr/bin/env bash

echo "[!] Linking ipython configuration"

mkdir -pv ~/.ipython/profile_default/
ln -vf python/ipython_config.py ~/.ipython/profile_default/ipython_config.py
