#!/usr/bin/env bash

echo "[!] Linking pentadactyl configuration"

ln -vf pentadactyl/pentadactylrc "$HOME"/.pentadactylrc
ln -svfT "$PWD"/pentadactyl "$HOME"/.pentadactyl

