#!/usr/bin/env bash

echo "[!] Setup epita specific config"

ln -vf $(dirname "$0")/systemd/jogsoul.service /usr/lib/systemd/system/
systemctl enable jogsoul
