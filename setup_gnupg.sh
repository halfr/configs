#!/usr/bin/env bash

echo "[!] Setting up gnupg"

mkdir -pv ~/.gnupg
ln -vf "$PWD"/gnupg/gpg-agent.conf "$HOME"/.gnupg/gpg-agent.conf
