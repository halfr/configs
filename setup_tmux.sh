#!/usr/bin/env bash

echo "[!] Linking tmux configuration"
ln -vf {tmux/,~/.}tmux.conf
