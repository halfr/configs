" Specify a directory for plugins
call plug#begin('~/.local/share/nvim/plugged')

" General
Plug 'airblade/vim-gitgutter'
Plug 'SirVer/ultisnips' | Plug 'honza/vim-snippets'
Plug 'Shougo/deoplete.nvim', { 'do': ':UpdateRemotePlugins' }
Plug 'sbdchd/neoformat'
Plug 'neomake/neomake'
autocmd! BufWritePost * Neomake
Plug 'Shougo/denite.nvim'
Plug 'google/vim-maktaba'
Plug 'Valloric/YouCompleteMe', { 'do': './install.py --clang-completer' }
let g:ycm_use_clangd = 0 " in case it's installed

" Bazel
Plug 'bazelbuild/vim-bazel'
Plug 'audebert/bazel-compilation-database'

" Go
Plug 'fatih/vim-go'
Plug 'zchee/deoplete-go', { 'do': 'make'}

" Python
let g:neoformat_enabled_python = ['yapf']

Plug 'zchee/deoplete-jedi'
" Python: formatting
autocmd FileType python setlocal equalprg=yapf
autocmd FileType python nnoremap <leader>y :0,$!yapf<Cr><C-o>

" Typescript
" REQUIRED: Add a syntax file. YATS is the best
Plug 'HerringtonDarkholme/yats.vim'
Plug 'mhartington/nvim-typescript', {'do': './install.sh'}

" Colorscheme
Plug 'noahfrederick/vim-noctu'

" Jsonnet
Plug 'google/vim-jsonnet'

" Rust
Plug 'rust-lang/rust.vim'
Plug 'racer-rust/vim-racer'

" Initialize plugin system
call plug#end()

" Colorscheme {{{
colorscheme noctu
set background=dark
" }}}

" Editzone {{{
set number
set relativenumber
set colorcolumn=80

" Highlight white spaces
highlight Trail ctermbg=red guibg=red
call matchadd('Trail', '\s\+$', 100)

" Setup tab
set shiftwidth=2
set softtabstop=2

" Highlight word under cursor
autocmd CursorMoved * exe printf('match IncSearch /\V\<%s\>/', escape(expand('<cword>'), '/\'))
" }}}
"
" On save (neoformat) {{{
" See https://github.com/sbdchd/neoformat
augroup fmt
  autocmd!
  autocmd BufWritePre * try | undojoin | Neoformat | catch /^Vim\%((\a\+)\)\=:E790/ | endtry
augroup END
" }}}

" Bindings {{{
" save
nnoremap <Space><Return> :w<Return>
" save and quit
nnoremap <Space><Backspace> :x<Return>
" rebind window to space
nnoremap <Space> <C-w>
" split
nnoremap <Space>- <C-w>s<Return>
" vsplit
nnoremap <Space>\| <C-w>v<Return>
" change window
nnoremap <Space><Space> <C-w>w
" ; -> :
nnoremap ; :
" }}}

" Backup related parameters {{{
set backup
set backupdir=~/.nvimtmp/backup
silent !mkdir -p ~/.nvimtmp/backup

set directory=~/.nvimtmp/temp
silent !mkdir -p ~/.nvimtmp/temp

set undofile
set undodir=~/.nvimtmp/undo
silent !mkdir -p ~/.nvimtmp/undo
" }}}

" Misc {{{
" Disable the mouse support that prevent pasting using middle click
set mouse=
" }}}

"" Plugins configuration {{{

" Rust {{{
au FileType rust nmap gd <Plug>(rust-def)
au FileType rust nmap gs <Plug>(rust-def-split)
au FileType rust nmap gx <Plug>(rust-def-vertical)
au FileType rust nmap <leader>gd <Plug>(rust-doc)
" }}}

" Plugin: vim-go {{{

" Go doc
au FileType go nmap <leader>gd <Plug>(go-doc)
au FileType go nmap ]d <Plug>(go-def)
au FileType go nmap <leader>gv <Plug>(go-doc-vertical)

" Go build
au FileType go nmap <leader>r <Plug>(go-run)
au FileType go nmap <leader>b <Plug>(go-build)
au FileType go nmap <leader>t <Plug>(go-test)
au FileType go nmap <leader>c <Plug>(go-coverage)

" Go code quality
au FileType go nmap <leader>gl <Plug>(go-lint)

" Go refactoring
au FileType go nmap <leader>gr <Plug>(go-rename)
" }}}

" Plugin: deoplete
let g:deoplete#enable_at_startup = 0

" Plugin: ultisnips
let g:UltiSnipsExpandTrigger="<c-j>"

" Plugin: denite {{{

" Define mappings
autocmd FileType denite call s:denite_my_settings()
function! s:denite_my_settings() abort
  nnoremap <silent><buffer><expr> <CR>
  \ denite#do_map('do_action')
  nnoremap <silent><buffer><expr> d
  \ denite#do_map('do_action', 'delete')
  nnoremap <silent><buffer><expr> p
  \ denite#do_map('do_action', 'preview')
  nnoremap <silent><buffer><expr> q
  \ denite#do_map('quit')
  nnoremap <silent><buffer><expr> i
  \ denite#do_map('open_filter_buffer')
  nnoremap <silent><buffer><expr> <Space>
  \ denite#do_map('toggle_select').'j'
endfunction


" Ag command on grep source
call denite#custom#var('grep', 'command', ['ag'])
call denite#custom#var('grep', 'default_opts',
		\ ['-i', '--vimgrep'])
call denite#custom#var('grep', 'recursive_opts', [])
call denite#custom#var('grep', 'pattern_opt', [])
call denite#custom#var('grep', 'separator', ['--'])
call denite#custom#var('grep', 'final_opts', [])

call denite#custom#var('file_rec', 'command',
	\ ['ag', '--follow', '--nocolor', '--nogroup', '-g', ''])

" Use git ls-files on git repository
call denite#custom#alias('source', 'file/rec/git', 'file/rec')
call denite#custom#var('file/rec/git', 'command', ['git', 'ls-files', '-co', '--exclude-standard'])
nnoremap <silent> <C-p> :Denite -start-filter `finddir('.git', ';') != '' ? 'file/rec/git' : 'file/rec'`<CR>

" Find pattern in current directory
nnoremap <Space>/ :Denite grep:.<CR>
" Search in buffers
nnoremap <Space>s :Denite buffer<CR>
" Search word under cursor
nnoremap <silent> <Space>* :Denite grep:.::<C-R><C-w><CR>

" Colors
call denite#custom#option('default', 'highlight_matched_char', 'Statement')
call denite#custom#option('default', 'highlight_matched_range', 'CursorColumn')
call denite#custom#option('default', 'highlight_preview_line', 'Underlined')

" }}}

" Plugin: vim-gitgutter {{{
highlight GitGutterAdd    ctermfg=4
highlight GitGutterChange ctermfg=4
highlight GitGutterDelete ctermfg=4
" }}}

" }}}

" vim:fdm=marker:
