#!/usr/bin/env bash

echo "[!] Linking git configuration"
ln -vf {vcs/,~/.}gitconfig

echo "[!] Linking mercurial configuration"
ln -vf {vcs/,~/.}hgrc
