# alisases.zsh

# use autojump-git
[ -f /etc/profile.d/autojump.zsh ] && source /etc/profile.d/autojump.zsh
unalias j 2>/dev/null

alias v="nvim"
alias py="ipython"
alias ipy="ipython"
alias most="most -c"
alias ..="cd .."

alias ls="\ls --color=auto"
alias rm="\rm --one-file-system"

alias mpvs="mpv --shuffle"
alias mpvf="mpv --fs"

alias i3lock="i3lock -d -c 111111 -n"

alias gdb='gdb -q'
alias cgdb="cgdb -q"

# typos
alias gi=git

function mkmv
{
    files=$(\ls -1)
    mkdir "$1"
    mv ${(f)files} "$1"
}
