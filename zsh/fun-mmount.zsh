function mmount {
    KNOWN_LABELS=(media_1To_0)
    for label in $KNOWN_LABELS; do
        if test -e /dev/disk/by-label/$label; then
            udisksctl mount -b /dev/disk/by-label/$label
            cd /run/media/halfr/$label
            return
        fi
    done
}
