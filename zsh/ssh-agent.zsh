typeset _ssh_env_cache

function _start_agent() {
  ssh-agent -s | sed 's/^echo/#echo/' >! $_ssh_env_cache
  chmod 600 $_ssh_env_cache
  source $_ssh_env_cache > /dev/null
}

# Get the filename to store/lookup the environment from
_ssh_env_cache="$HOME/.ssh/environment"

if [[ -f "$_ssh_env_cache" ]]; then
  # Source SSH settings, if applicable
  source $_ssh_env_cache > /dev/null
  ps x | grep ssh-agent | grep -q $SSH_AGENT_PID || {
    _start_agent
}
else
  _start_agent
fi
