# fun-pkg-upload.zsh
# Upload an Arch Linux package to my custom repository

pkg-upload() {
    makepkg -f
    latest_pkg=$(echo *.pkg.tar.xz(od[1]))
    echo "[+] Uploading $latest_pkg to repo.halfr.net"
    scp $latest_pkg halfr.net:/srv/http/repo.halfr.net
    ssh halfr.net "cd /srv/http/repo.halfr.net && repo-add halfr.db.tar.gz $latest_pkg"
}
