#!/usr/bin/env zsh

# Set zsh key binds to emacs default
bindkey -e

_recall() {
    [[ -z $BUFFER ]] && zle up-history
    zle accept-line
};

zle -N _recall
bindkey '^M' _recall  # Enter

# Edit lastly edited file in the current folder
# '-s' binds one key sequence to another
bindkey -s '\e^M' '^Qvim *(.om[1])^M' # Alt+Enter

# Run make
bindkey -s '\e[15~' '^Qmake^M'  # F5

# Bash word splitting, useful for word kills on paths
autoload -U select-word-style
select-word-style bash
