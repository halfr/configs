#!/bin/zsh

autoload -U promptinit
fpath+=(~/.zsh/prompts/)
setopt PROMPT_SUBST
promptinit
set_prompt halfr
