function upload {
    what=$1
    shift
    [[ $1 = to ]] && shift
    if [[ $# -eq 0 ]]; then
        case $what in
            (*.torrent)
                upload $what to seedbox
                ;;
        esac
    else
        case $1 in
            (seedbox)
                rsync --remove-source-files -Pha $what vm-shared:torrents/
                ;;
            (halfr.net|*)
                rsync -Pha $what halfr.net:incoming/
                ;;
        esac
    fi
}
