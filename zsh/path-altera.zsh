# Extra path for Altera software

PATH="$PATH:/home/halfr/opt/altera/nios2eds/bin"
PATH="$PATH:/home/halfr/opt/altera/nios2eds/bin/gnu/H-x86_64-pc-linux-gnu/bin"
PATH="$PATH:/home/halfr/opt/altera/nios2eds/sdk2/bin"

export QUARTUS_ROOTDIR=/home/halfr/opt/altera/
export SOCEDS_DEST_ROOT=/home/halfr/opt/altera/embedded
