function liftoff {
    mountpoint=$(stat -c '%m' $PWD)
    cd "$mountpoint/.."
    umount "$mountpoint"
}
