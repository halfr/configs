# fun-mkcd.zsh

mkcd () {
    if (( ARGC != 1 )); then
        echo 'usage: mkcd <new-directory>'
        return 1
    fi

    if [[ ! -d "$1" ]]; then
        command mkdir -p "$1"
    else
        echo "\`$1' already exists: cd-ing."
    fi
    builtin cd "$1"
}
