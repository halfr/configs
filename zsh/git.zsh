# tools related to git

git-authors() {
    for f in $(git ls-files); do
        git blame --line-porcelain "$f" | sed -n 's/^author //p'
    done | sort | uniq -c | sort -rn
}

cdg() {
    cd $(git rev-parse --show-toplevel)
}
