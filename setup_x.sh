#!/usr/bin/env bash

echo "[!] Linking X environement configuration"

ln -vf {X/,~/.}Xdefaults
ln -vf {X/,~/.}xinitrc
ln -vf {X/,~/.}Xmodmap

echo "[!] Installing font (termsynuh)"
mkdir -p $HOME/.local/share/fonts/
if ! test -d $HOME/.local/share/fonts/termsynuh; then
  git clone https://bitbucket.org/halfr/termsynuh $HOME/.local/share/fonts/termsynuh
fi
make -C $HOME/.local/share/fonts/termsynuh
