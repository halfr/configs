#!/usr/bin/env bash

echo "[!] Setting up i3"

mkdir -pv ~/.i3
ln -vf "$PWD"/i3/config ~/.i3/config

mkdir -pv ~/.config/i3status/
ln -vf "$PWD"/i3status/config ~/.config/i3status/config
