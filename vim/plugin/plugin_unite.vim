" plugin_unite.vim

" Configuration
let g:unite_source_rec_async_command = 'ag --follow --nocolor --nogroup --hidden -g ""'

" Bindings

" Search files in current dir
nnoremap <C-p> :Unite -start-insert file_rec/async<CR>
" Find pattern in current directory
nnoremap <Space>/ :Unite grep:.<CR>
" Search in buffers
nnoremap <Space>s :Unite buffer<CR>
" Search word under cursor
nnoremap <silent> <Space>* :Unite grep:.::<C-R><C-w><CR>

