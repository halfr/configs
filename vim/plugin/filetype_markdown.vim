" filetype_markdown.vim

autocmd BufEnter *.md set filetype=mkd
let g:vim_markdown_folding_disabled=1
