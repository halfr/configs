" syntax.vim

syntax enable

" Python specific options
let python_highlight_all = 1
