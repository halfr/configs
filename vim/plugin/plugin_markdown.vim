" plugin_markdown.vim
" Configuration for the vim-markdown plugin

" Do not fold
let g:vim_markdown_folding_disabled=1
