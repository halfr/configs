" filetype.vim
" Filetype related configuration.

filetype plugin indent on

" Missing extension
au BufRead,BufNewFile *.md set filetype=mkd
