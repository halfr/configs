" plugin_python-mode.vim
" Configuration for the python-mode plugin

" I mostly use python3
let g:pymode_python = "python3"

" Do not run lint on each write
let g:pymode_lint_on_write = 0

" Disable folding
let g:pymode_folding = 0

" Disable rope
let g:pymode_rope_lookup_project = 0
let g:pymode_rope = 0
