#!/usr/bin/env bash

./setup_ssh.sh
./setup_tmux.sh
./setup_vcs.sh
./setup_zsh.sh
./setup_vim.sh
./setup_python.sh
./setup_x.sh
./setup_pentadactyl.sh
./setup_i3.sh
