#!/usr/bin/env bash

echo "[!] Linking ssh configuration"

mkdir -pv ~/.ssh
ln -vf {ssh,~/.ssh}/config
