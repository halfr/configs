#!/usr/bin/env bash

echo "[!] Linking zsh configuration"

ln -svf "$PWD"/zsh/zshrc "$HOME"/.zshrc
ln -svfT "$PWD"/zsh "$HOME"/.zsh
