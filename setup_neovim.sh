#!/bin/sh

echo "[+] Downloading vim-plug"
curl -fLo ~/.local/share/nvim/site/autoload/plug.vim --create-dirs \
	    https://raw.githubusercontent.com/junegunn/vim-plug/master/plug.vim

echo "[+] Linking neovim configuration"
mkdir -pv ~/.config/nvim/
ln -svf $PWD/neovim/init.vim ~/.config/nvim/init.vim

echo "[+] Bootstrapping neovim configuration"
nvim +PlugInstall
nvim +UpdateRemotePlugin
