#!/usr/bin/env bash

echo "[!] Linking vim configuration"

ln -svf {$PWD/vim/,~/.}vimrc
ln -svfT $PWD/vim ~/.vim

mkdir -p ~/.vim/bundle
(cd ~/.vim/bundle; git clone http://github.com/gmarik/Vundle.vim)
